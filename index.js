// Use the "require" directive to load the express module/package

	const express = require('express');

	// create an application using express function

	const app = express();

	// for our application server to run, we need a port to listen.
	const port = 3000;

	// Methods used from expressJS are middlewares.
	// API management is one of the common applications of middlewares
	// allows your app to read json data.
	app.use(express.json());

	// allows your app to read your data from forms.
	app.use(express.urlencoded({extended: true}));


// [Section] ROUTES
	// Express has methods corresponding to each HTTP method
	// This route expects to recieve a GET request at the base pf the URI "/".
	// The full base URI for our goal local application for this route will be at "http//localhost:300".

	app.get("/", (req,res) =>{

		res.send("Hello W0rld.");
	})

	// Create a "GET" Route
	 //this route expects to recieve a GET request at URI "/hello"

	 	app.get("/hello-get", (req,res) => {
	 		res.send("Hello from the /hello endpoint!");
	 	})


	// Create a "POST" Route
	 //this route expects to recieve a POST request at URI "/hello".
	 //req.body contains the contents/data of the request body

	 	app.use("/hello-post", (req,res) => {
	 		res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`);
	 	})


	// Create an array to use as our mock Database
	 //an array that will store our user objects when the "/signup" route is accessed
		let users = [];
		app.post("/signup", (req,res) => {
			console.log(req.body)

			if(req.body.userName !== "" && req.body.password !== ""){
				users.push(req.body)
				res.send(`User ${req.body.userName} is successesfully registered!`)
			}else {
				res.send(`Empty Username and Password. Please try again.`)
			}
		})

		app.get("/users", (req,res) => {
			res.send(users);

			// if(req.body.userName !== "" && req.body.password !== ""){
			// 	users.push(req.body)
			// 	res.send(`User ${req.body.userName} is successesfully registered!`)
			// }else {
			// 	res.send(`Empty Username and Password. Please try again.`)
			// }
		})


	 //the route expects tp recieve a "PUT" request at the URI "/change-password"

	 	app.put("/change-password", (req, res) => {
	 		let message;

	 		for(let x = 0; x < users.length; x++){

	 			if(req.body.userName == users[x].userName){
	 				users[x].password = req.body.password

	 				message = `User ${req.body.userName}'s password has been updated.`
	 				break;

	 			// if user does not exist
	 			}else{
	 				message = "User does no exist!"
	 			}
	 		}

	 		res.send(message);
	 	})

	app.listen(port, () => console.log(`Server tunning at port ${port}.`));
